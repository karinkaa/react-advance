import { Avatar, Card, CardContent, CardHeader, IconButton, makeStyles, Typography } from "@material-ui/core";
import { Favorite } from "@material-ui/icons";
import React, { FunctionComponent, useState } from "react";
import { MessageType } from "../../data/types/message-type";
import { formatDate } from "../../helper";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 600,
    background: "bisque",
    marginBottom: 50
  },
  avatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
  },
  iconButton: {
    color: (props) => props ? "red" : "dimgray"
  }
}));

interface MessageProps {
  message: MessageType;
}

const Message: FunctionComponent<MessageProps> = ({ message }) => {
  const { avatar, user, text, createdAt, editedAt } = message;
  const [isLiked, setIsLiked] = useState<boolean>(false);

  const classes = useStyles(isLiked);

  const handleClick = () => {
    setIsLiked(!isLiked);
  }

  return (
    <Card className={"message " + classes.root}>
      <CardHeader
        action={
          <IconButton
            className={"message-like " + classes.iconButton}
            onClick={handleClick}
          >
            <Favorite />
          </IconButton>
        }
        avatar={
          <Avatar
            className={"message-user-avatar " + classes.avatar}
            alt="avatar"
            src={avatar}
          />
        }
        title={
          <Typography variant="h6" className={"message-user-name"}>
            {user}
          </Typography>
        }
        subheader={
          <Typography variant="subtitle2" className={"message-time"}>
            {formatDate(editedAt || createdAt)}
          </Typography>
        }
      />
      <CardContent>
        <Typography variant="body1" className={"message-text"}>
          {text}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default Message;
