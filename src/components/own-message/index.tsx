import { Card, CardContent, CardHeader, IconButton, makeStyles, Typography } from "@material-ui/core";
import { Delete, Update } from "@material-ui/icons";
import React, { FunctionComponent } from "react";
import { MessageType } from "../../data/types/message-type";
import { formatTime } from "../../helper";
import EditModal from "../edit-modal";

const useStyles = makeStyles({
  root: {
    maxWidth: 600,
    background: "aliceblue",
    marginBottom: 25,
    marginLeft: "auto"
  },
  iconButton: {
    color: "dimgray"
  }
});

interface OwnMessageProps {
  message: MessageType;
  removeMessage: (id: string) => void;
  saveMessage: (id: string | undefined, text: string) => void;
};

const OwnMessage: FunctionComponent<OwnMessageProps> = ({ message, removeMessage, saveMessage }) => {
  const classes = useStyles();
  const { id, text, createdAt, editedAt } = message;
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickUpdate = () => {
    handleOpen();
  }

  return (
    <Card className={"own-message " + classes.root}>
      <CardHeader
        action={
          <>
            <IconButton
              className={"message-edit " + classes.iconButton}
              onClick={handleClickUpdate}
            >
              <Update />
            </IconButton>

            <EditModal id={id} text={text} open={open} handleClose={handleClose} saveMessage={saveMessage} />

            <IconButton
              className={"message-delete"}
              onClick={() => removeMessage(id)}
            >
              <Delete />
            </IconButton>
          </>
        }
        subheader={
          <Typography variant={"subtitle2"} className={"message-time"}>
            {formatTime(editedAt || createdAt)} {editedAt && "(edited)"}
          </Typography>
        }
      />
      <CardContent>
        <Typography variant={"body1"} className={"message-text"}>
          {text}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default OwnMessage;
