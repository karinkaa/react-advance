import { AppBar, IconButton, makeStyles, Toolbar, Typography } from "@material-ui/core";
import { AcUnit } from "@material-ui/icons";
import React, { FunctionComponent } from "react";
import { MessageType } from "../../data/types/message-type";
import { getFullDateTime } from "../../helper";

const useStyles = makeStyles({
  typo: {
    marginRight: 50,
    "&:last-child": {
      marginRight: 0
    }
  },
  div: {
    flexGrow: 1
  }
});

interface HeaderProps {
  messages: MessageType[];
}

const Header: FunctionComponent<HeaderProps> = ({ messages }) => {
  const classes = useStyles();

  const participantsCount = new Set(messages.map(item => item.userId)).size;
  const messagesCount = messages.length;
  const lastMessageDate = messagesCount ? messages[messagesCount - 1].createdAt : "";

  return (
    <AppBar position="fixed" className="header">
      <Toolbar>
        <IconButton edge="start">
          <AcUnit />
        </IconButton>

        <Typography variant="h6" className={"header-title " + classes.typo}>
          Chat
        </Typography>

        <Typography variant="subtitle1" className={"header-users-count " + classes.typo}>
          {participantsCount} participants
        </Typography>

        <Typography variant="subtitle1" className={"header-messages-count " + classes.typo}>
          {messagesCount} messages
        </Typography>

        <div className={classes.div}></div>

        {messagesCount > 0 && (
          <Typography variant="subtitle1" className={"header-last-message-date " + classes.typo}>
            last message at {getFullDateTime(lastMessageDate)}
          </Typography>
        )}
      </Toolbar>
    </AppBar>
  );
}

export default Header;
