import { makeStyles } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { Route, Routes } from "react-router-dom";
import { RootState } from "../../redux";
import Chat from "../chat";
import Login from "../login";

const useStyles = makeStyles({
  root: {
    minHeight: "100vh",
    background: "#313537"
  }
});

function App() {
  const classes = useStyles();
  const role = useSelector((state: RootState) => state.profile.role);

  return (
    <div className={classes.root}>
      <Routes>
        {
          role ?
            <Route path="/" element={<Chat />} /> :
            <Route path="/*" element={<Login />} />
        }
      </Routes>
    </div>
  );
}

export default App;
