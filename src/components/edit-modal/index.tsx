import { Button, TextField } from "@material-ui/core";
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import { Check, Close } from "@material-ui/icons";
import React, { FunctionComponent, useEffect, useState } from 'react';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #303F9F',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  text: {
    background: "aliceblue",
    maxHeight: 55,
    margin: "0 20px 20px 0",
    borderRadius: 5,
    overflowY: "hidden"
  },
  btn: {
    height: 50,
    width: "35%",

    "&:last-child": {
      float: "right"
    }
  }
}));

interface EditModalProps {
  id?: string;
  text: string | undefined;
  open: boolean;
  handleClose: () => void;
  saveMessage: (id: string | undefined, text: string) => void;
};

const EditModal: FunctionComponent<EditModalProps> = ({ id, text, open, handleClose, saveMessage }) => {
  const classes = useStyles();
  const modalShown = open ? "modal-shown" : "";
  const [value, setValue] = useState<string>(text || "");

  useEffect(() => {
    setValue(text || "");
  }, [text]);

  const handleClick = () => {
    const trimValue = value?.trim();

    if (trimValue) {
      saveMessage(id, trimValue);
      setValue("");
      handleClose();
    }
  }

  return (
    <Modal
      className={`edit-message-modal ${modalShown} ${classes.modal}`}
      open={open}
      onClose={handleClose}
    >
      <div className={classes.paper}>
        <TextField
          className={"edit-message-input " + classes.text}
          variant={"outlined"}
          fullWidth
          autoFocus
          value={value}
          onChange={(e) => setValue(e.target.value)}
          onKeyDown={(e) => e.key === "Enter" && handleClick()}
        />
        <Button
          className={"edit-message-close " + classes.btn}
          variant={"contained"}
          endIcon={<Close />}
          onClick={handleClose}
        >
          Close
        </Button>
        <Button
          className={"edit-message-button " + classes.btn}
          variant={"contained"}
          color={"primary"}
          endIcon={<Check />}
          onClick={handleClick}
        >
          Edit
        </Button>
      </div>
    </Modal>
  );
}

export default EditModal;
