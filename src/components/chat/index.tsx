import { CircularProgress, makeStyles, Toolbar } from "@material-ui/core";
import React, { useEffect, useRef } from "react";
import { MessageType } from "../../data/types/message-type";
import { createMessage } from "../../helper";
import { useGetMessagesQuery, useRemoveMessageMutation, useSaveMessageMutation, useUpdateMessageMutation } from "../../redux";
import Header from "../header";
import MessageInput from "../message-input";
import MessageList from "../message-list";

const useStyles = makeStyles({
  progress: {
    position: "absolute",
    left: "50%",
    top: "50%"
  }
});

const AlwaysScrollToBottom = () => {
  const elementRef = useRef<HTMLDivElement>();
  useEffect(() => {
    return elementRef?.current?.scrollIntoView();
  });

  // @ts-ignore: Unreachable code error
  return <div ref={elementRef} />;
};

const Chat = () => {
  const classes = useStyles();

  const { data: messages = [], isLoading } = useGetMessagesQuery();
  const [saveMessage] = useSaveMessageMutation();
  const [updateMessage] = useUpdateMessageMutation();
  const [removeMessage] = useRemoveMessageMutation();

  const handleRemoveMessage = async (id: string) => {
    await removeMessage(id).unwrap();
  }

  const handleSaveMessage = async (id: string | undefined, text: string) => {
    const msg = messages.find((item: MessageType) => item.id === id) || createMessage(text);

    if (id) {
      await updateMessage({ ...msg, text, editedAt: new Date().toString() });
    } else {
      await saveMessage(msg).unwrap();
    }
  }

  return (
    <div className={"chat"}>
      <Header messages={messages} />
      <Toolbar />
      <Toolbar />
      <div className={classes.progress}>
        {isLoading && <CircularProgress color={"primary"} />}
      </div>
      <MessageList messages={messages} removeMessage={handleRemoveMessage} saveMessage={handleSaveMessage} />
      <Toolbar />
      <MessageInput saveMessage={handleSaveMessage} />
      <AlwaysScrollToBottom />
    </div>
  );
}

export default Chat;
