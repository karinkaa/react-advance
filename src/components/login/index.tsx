import { Avatar, Button, Container, Grid, TextField, Typography } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import LockOpen from '@material-ui/icons/LockOpen';
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { changeRole } from "../../redux/profile-slice";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(25),
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  avatar: {
    margin: theme.spacing(2),
    backgroundColor: "teal"
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(5)
  },
  submit: {
    marginTop: theme.spacing(5),
    height: 55
  }
}));

const Login = () => {
  const classes = useStyles();
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const handleClick = () => {
    // todo mock
    if (login === "admin" && password === "admin") {
      dispatch(changeRole("admin"));
    }
    dispatch(changeRole("user"));
  }

  return (
    <Container component="main" maxWidth="xs" className={classes.root}>
      <Avatar className={classes.avatar}>
        <LockOpen />
      </Avatar>

      <Typography variant="h5">
        Log In
      </Typography>

      <form className={classes.form} noValidate>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              label="Login"
              value={login}
              required
              fullWidth
              autoFocus
              variant="outlined"
              onChange={(e) => setLogin(e.target.value)}
            />
          </Grid>

          <Grid item xs={12}>
            <TextField
              label="Password"
              type="password"
              value={password}
              required
              fullWidth
              autoFocus
              variant="outlined"
              onChange={(e) => setPassword(e.target.value)}
            />
          </Grid>
        </Grid>

        <Link to={"/"}>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleClick}
          >
            LogIn
          </Button>
        </Link>
      </form>
    </Container>
  )
}

export default Login;
