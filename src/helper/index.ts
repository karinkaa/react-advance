import { v4 as uuidv4 } from 'uuid';
import { MessageType } from "../data/types/message-type";
import { DEFAULT_AVATAR, USER_ID, USER_NAME } from "../data/user-data";

export function saveObjectItemTo<T extends { id: string }>(list: T[], item: T): T[] {
  const index = list.findIndex((el) => el.id === item.id);
  let newList;

  if (index === -1) {
    newList = [...list, item];
  } else {
    newList = [
      ...list.slice(0, index),
      item,
      ...list.slice(index + 1)
    ];
  }

  return newList;
}

export function createMessage(text: string): MessageType {
  return {
    id: uuidv4(),
    userId: USER_ID,
    avatar: DEFAULT_AVATAR,
    user: USER_NAME,
    text,
    createdAt: new Date().toString(),
    editedAt: ""
  };
}

export function formatTime(date: string): string {
  const dateTime = new Date(date);
  const hours = dateTime.getHours();
  const minutes = dateTime.getMinutes();

  return `${getFormatNumber(hours)}:${getFormatNumber(minutes)}`;
}

export function formatDate(date: string): string {
  const dateTime = new Date(date);

  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  const dayOfWeek = days[dateTime.getDay()];
  const month = months[dateTime.getMonth()];
  const day = dateTime.getDate();

  return `${dayOfWeek}, ${day} ${month}`;
}

export function getDividerText(date: string): string {
  const yesterdayDate = new Date();
  yesterdayDate.setDate(yesterdayDate.getDate() - 1);

  const today = formatDate(new Date().toString());
  const yesterday = formatDate(yesterdayDate.toString());
  const nowDate = formatDate(date);

  if (nowDate === today) {
    return "Today";
  } else if (nowDate === yesterday) {
    return "Yesterday";
  } else {
    return nowDate;
  }
}

function getFormatNumber(number: number): string {
  return number < 10 ? "0" + number : number + "";
}

export function getFullDateTime(date: string): string {
  const dateTime = new Date(date);
  const day = getFormatNumber(dateTime.getDate());
  const month = getFormatNumber(dateTime.getMonth() + 1);
  const year = dateTime.getFullYear();
  const hours = getFormatNumber(dateTime.getHours());
  const minutes = getFormatNumber(dateTime.getMinutes());
  const seconds = getFormatNumber(dateTime.getSeconds());

  return `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;
}
