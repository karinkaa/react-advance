import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { HttpMethod } from "../data/http-method";
import { MessageType } from "../data/types/message-type";

export const messagesApi = createApi({
  reducerPath: "messagesApi",
  tagTypes: ["Messages"],
  baseQuery: fetchBaseQuery({ baseUrl: "http://localhost:3001/" }),
  endpoints: (build) => ({
    getMessages: build.query<MessageType[], void>({
      query: () => "messages",
      providesTags: (result) => result
        ? [...result.map(({ id }) => ({ type: "Messages" as const, id })), { type: "Messages", id: "LIST" }]
        : [{ type: "Messages", id: "LIST" }],
    }),
    saveMessage: build.mutation({
      query: (body) => ({
        url: "messages",
        method: HttpMethod.POST,
        body,
      }),
      invalidatesTags: [{ type: "Messages", id: "LIST" }]
    }),
    updateMessage: build.mutation<void, Pick<MessageType, "id"> & Partial<MessageType>>({
      query: ({ id, ...message }) => ({
        url: `messages/${id}`,
        method: HttpMethod.PATCH,
        body: message,
      }),
      invalidatesTags: [{ type: "Messages", id: "LIST" }]
    }),
    removeMessage: build.mutation({
      query: (id) => ({
        url: `messages/${id}`,
        method: HttpMethod.DELETE,
      }),
      invalidatesTags: [{ type: "Messages", id: "LIST" }]
    })
  })
});

export const { useGetMessagesQuery, useSaveMessageMutation, useRemoveMessageMutation, useUpdateMessageMutation } = messagesApi;
