import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface ProfileState {
  role: string
}

const initialState: ProfileState = {
  role: ""
};

const profileSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    changeRole(state, action: PayloadAction<string>) {
      state.role = action.payload;
    }
  },
})

export const { changeRole } = profileSlice.actions;
export default profileSlice.reducer;
