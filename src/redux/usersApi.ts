import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { UserType } from "../data/types/user-type";

export const usersApi = createApi({
  reducerPath: "usersApi",
  tagTypes: ["Users"],
  baseQuery: fetchBaseQuery({ baseUrl: "http://localhost:3001/" }),
  endpoints: (build) => ({
    getUsers: build.query<UserType[], void>({
      query: () => "users",
      providesTags: (result) => result
        ? [...result.map(({ id }) => ({ type: "Users" as const, id })), { type: "Users", id: "LIST" }]
        : [{ type: "Users", id: "LIST" }],
    }),
    findUser: build.query<UserType, { login: string, password: string }>({
      query: ({ login, password }) => `users?login=${login}&password=${password}`
    })
  })
});

export const { useGetUsersQuery, useFindUserQuery } = usersApi;
