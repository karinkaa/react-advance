export type UserType = {
  id: string,
  login: string,
  password: string
};
