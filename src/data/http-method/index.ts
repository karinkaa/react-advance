export const HttpMethod = {
  POST: "POST",
  PATCH: "PATCH",
  DELETE: "DELETE"
};
